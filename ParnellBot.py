import datetime, importlib.util, socket, sys, time, json, asyncio
from termcolor import colored
from random import randint
from discord.ext import commands

description = "Discord bot which sends idiotic and out of context quotes from Rob Parnell. " \
              "Quotes are sent as a response to messages containing specific key words as well " \
              "as random quotes to brighten up the day of all members of the discord chat."
name = "Parnell Bot"
author = "Seann"
version = "1.2"
bot_prefix = ""
rob = commands.Bot(description=description, command_prefix=bot_prefix)

with open('settings.json') as f:
    settings = json.load(f)


@rob.event
async def on_ready():
    print(colored("%s v%s" % (name, version), 'red'))
    print(colored("Created by: %s" % author, "blue"))
    print(colored("Ready!", "green"))
    await send_spam(rob.get_channel("YOUR_CHANNEL_ID"))


@rob.event
async def send_spam(channel):
    while True:
        timeout = randint(10, 24)
        msg = settings['quote']['spam'][randint(0, len(settings['quote']['spam']) - 1)]
        await rob.send_message(channel, "```prolog\n\"%s\"```" % msg)
        await asyncio.sleep(timeout * 60)


@rob.event
async def on_message(message):
    sent = msg = False
    if message.author == rob.user or message.author.bot:
        return

    if message.content.startswith("addQuote "):
        if is_admin(message.author.id):
            qu = message.content.replace("addQuote ", "")
            g = qu.split(' ')
            group = g[len(g) - 1]
            qu = qu.replace(" %s" % group, "")
            if group in settings['quote']:
                settings['quote'][group].append(qu)
                update_settings()
                msg = "%s added \"%s\" to %s" % (message.author.name, qu, group.capitalize())
            else:
                msg = "Could not find group: \"%s\"" % group
        else:
            msg = "Only admins can add Quotes!"
    elif message.content.startswith("addAdmin "):
        if is_admin(message.author.id):
            member = message.content.replace("addAdmin ", "")
            for user in message.server.members:
                if user.name.lower() == member.lower():
                    if is_admin(user.id):
                        msg = "%s is already an Admin!" % user.name
                    else:
                        settings['admin'].append("%s" % user.id)
                        update_settings()
                        msg = "%s made %s an Admin!" % (message.author.name, user.name)
                break
            if not msg:
                msg = "Could not find user: %s" % member
        else:
            msg = "Only admins can add Admins!"
    else:
        for key in settings['keyword']:
            if not sent:
                for kw in settings['keyword'][key]:
                    if not sent:
                        if kw in message.content.lower():
                            msg = "\"%s\"" % settings['quote'][key][randint(0, len(settings['quote'][key])-1)]
                            sent = True
                            break
    if msg:
        await rob.send_message(message.channel, "```prolog\n%s```" % msg)


def is_admin(user_id):
    for a in settings['admin']:
        if user_id == a:
            return True
    return False


def update_settings():
    file = open('settings.json', 'w+')
    file.write(json.dumps(settings, indent=4, sort_keys=True))
    file.close()


rob.run("YOUR_TOKEN_HERE")